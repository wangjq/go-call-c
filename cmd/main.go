package main

/*
#cgo CFLAGS: -I../c
#cgo LDFLAGS: -L../lib -lhi
#include "hi.h"
*/
import "C"

import "fmt"

func main(){
	C.hi()
	fmt.Println("Hello c, welcome to go!")
}